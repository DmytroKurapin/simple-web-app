import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-edit-review',
  templateUrl: './add-edit-review.component.html',
  styleUrls: ['./add-edit-review.component.sass']
})
export class AddEditReviewComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('review-data') reviewData: { name: string, comment: string } | null;
  @Output() edit: EventEmitter<{ name: string, comment: string }> = new EventEmitter();
  @Output() addReview: EventEmitter<{ name: string, comment: string }> = new EventEmitter();
  public form: FormGroup;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.compose([Validators.required])],
      comment: ['', Validators.compose([Validators.required])],
    });
    if (this.reviewData) {
      this.form.setValue(this.reviewData);
    }
    console.dir(this.form.get(name));
  }

  get buttonName(): string {
    return !this.reviewData || this.reviewData.name === '' ? 'Add' : 'Edit';
  }

  sendData() {
    const { value } = this.form;
    this.form.reset();
    return this.buttonName === 'Add' ? this.addReview.emit(value) : this.edit.emit(value);
  }
}
