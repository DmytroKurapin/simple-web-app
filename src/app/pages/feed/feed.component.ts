import { Component, OnInit } from '@angular/core';
import { UserReviewsService } from '../../shared/services/user-reviews.service';
import { ReviewObject } from '../../shared/review-object';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.sass']
})
export class FeedComponent implements OnInit {
  public editingReview: ReviewObject | null;

  constructor(public userReviewsService: UserReviewsService) {
  }

  ngOnInit() {
  }

  public editReview(reviewsObj: ReviewObject) {
    this.editingReview = reviewsObj;
  }

  public updateWithEditedReview(data: { name: string, comment: string }) {
    this.userReviewsService.editReview(Object.assign(this.editingReview, data));
    this.editingReview = null;
  }
}
