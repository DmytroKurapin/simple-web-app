export interface ReviewObject {
  id: number;
  name: string;
  comment: string;
  avatar: string;
}
