import { Injectable } from '@angular/core';
import { ReviewObject } from '../review-object';
import * as imgGen from '@dudadev/random-img';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserReviewsService {
  private readonly _listOfReviews: BehaviorSubject<ReviewObject[]>;
  private latestUserId = 0;

  get listOfReviews(): BehaviorSubject<ReviewObject[]> {
    return this._listOfReviews;
  }

  setListOfReviews(arr: ReviewObject[]) {
    this._listOfReviews.next(arr);
    localStorage.setItem('reviews', JSON.stringify(arr));
  }

  constructor() {
    const existedReviews = JSON.parse(localStorage.getItem('reviews') || '[]');
    if ( existedReviews.length !== 0 ) {
      this.latestUserId = existedReviews[existedReviews.length - 1].id + 1;
    }
    this._listOfReviews = new BehaviorSubject<ReviewObject[]>(existedReviews);
  }

  public addNewReview(formObj: { name: string, comment: string }): void {
    imgGen().then(avatarURL => {
      const currReviews = this.listOfReviews.value;
      currReviews.push({
        ...formObj,
        avatar: avatarURL,
        id: this.latestUserId,
      });
      this.setListOfReviews(currReviews);
      this.latestUserId++;
    });
  }

  public editReview(obj: ReviewObject): void {
    const currReviews = this.listOfReviews.value;
    const updatedReviews = currReviews.map(reviewObj => reviewObj.id === obj.id ? obj : reviewObj);
    this.setListOfReviews(updatedReviews);
  }

    public deleteReview(id: number): void {
    const currReviews = this.listOfReviews.value;
    const idx = currReviews.findIndex(reviewObj => reviewObj.id === id);
    if (idx > -1) {
      currReviews.splice(idx, 1);
      this.setListOfReviews(currReviews);
    }
  }
}
